       IDENTIFICATION DIVISION. 
       PROGRAM-ID. TRADER.
       AUTHOR.  RENU.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT TRADER-FILE ASSIGN TO "trader1.dat"
           ORGANIZATION IS LINE SEQUENTIAL.
           SELECT  TRADER-REPORT-FILE ASSIGN TO "trader.rpt"
           ORGANIZATION IS SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION. 
       FD  TRADER-FILE.
       01  TRADER-DETATL.
           88   END-OF-MEMBER-FILE      VALUE HIGH-VALUE.
           05   PROVINCE-ID             PIC X(2).
           05   PINCOME                PIC X(14).
           05   MEMBER-ID               PIC X(8).
           05   MEMBER-INCOME           PIC X(14).
       FD TRADER-REPORT-FILE.
       01 PRN-LINE                       PIC X(44).

       WORKING-STORAGE SECTION.
       01 PAGE-HEADER.
           05 FILLER PIC x(44)
           VALUE "Trader".
       01 PAGE-FOOTING.
           05 FILLER      PIC X(15) VALUE  SPACES.
           05 FILLER       VALUE  "PROVINCE: ".
           05 PRN-PAGE-NUM    PIC Z9.
       01 COLUMN-HEADING    PIC X(50)
           VALUE "P INCOME: ".
       01 TRADER-DETATL-LINE.
           05 FILLER      PIC X VALUE SPACES.
           05 PRN-PROVINCE-ID       PIC X(5).
           05 FILLER                PIC X(4) VALUE SPACES.
           05 PRN-PINCOME           PIC X(20).
           05 FILLER                PIC XX VALUE SPACES.
           05 PRN-MEMBER-ID         PIC X.
           05 FILLER                PIC X(4) VALUE SPACES.
           05 PRN-MEMBER-INCOME     PIC X.
       01 LINE-COUNT                 PIC 99 VALUE ZEROS.
           88 NEW-PAGE-REQUIRED     VALUE 40 THRU 99.
       01 PAGE-COUNT                 PIC 99 VALUE ZEROS.

       PROCEDURE DIVISION.
       BEGIN.
           OPEN INPUT TRADER-FILE
           OPEN OUTPUT TRADER-REPORT-FILE

     

           CLOSE TRADER-FILE
           CLOSE TRADER-REPORT-FILE
           GOBACK
           .





   
           




            